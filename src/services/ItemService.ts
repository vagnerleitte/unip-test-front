import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ItemService {
    constructor(private httpClient: HttpClient) { 
        this.list();
    }

    list() {
        return this.httpClient.get('http://localhost:3000/items');
    }
    
}