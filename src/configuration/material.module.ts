import { NgModule } from "@angular/core";
import { MatIconModule, MatMenuModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatExpansionModule, MatTableModule } from "@angular/material";

@NgModule({
    imports: [
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatExpansionModule,
        MatTableModule,
    ],
    exports: [
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatExpansionModule,
        MatTableModule

    ]
})
export class MaterialModule { }